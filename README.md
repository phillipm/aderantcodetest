# README #

Download and compile. No additional steps necessary

### What is this repository for? ###

This is my implementation of the Aderant coding test.
The test is to re-assemble string fragments that have overlaps,
i.e. the suffix of one string matches the prefix of another. The
overlap can be of any length.

### Notes

I took inspiration from a [Stanford University online course](https://www.coursera.org/learn/dna-sequencing/lecture/iD20p/practical-implementing-greedy-shortest-common-superstring).
The lecture provided the idea of creating an edge graph of the overlapping strings.
I did not use exactly their approach because it seems quite expensive - recalculating
the overlap for all edges every iteration through the main loop. My approach only
recalculates the overlap of nodes affected by joining two strings.

The test did not stipulate any kind of performance requirement so I have not looked to
optimise the performance in any other way. If the code was for large datasets and needed
to be highly performant I would spend more time analysing memory and cpu usage and
exploring alternative approaches.
