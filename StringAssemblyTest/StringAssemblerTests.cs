﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringAssembly;

namespace StringAssemblyTest
{
    [TestClass]
    public class StringAssemblerTests
    {
        [TestMethod]
        public void AssemblyFullyOverlappingSegmentsTestWithNone()
        {
            // setup
            var fragments = new string[] { };

            // execute
            var result = StringAssembler.GreedyAssembleStrings(fragments);

            // test - can't guarantee end result, so just ensure all fragments are contained
            Assert.AreEqual(string.Empty, result);
        }
        [TestMethod]
        public void AssemblyFullyOverlappingSegmentsTestWithOne()
        {
            // setup
            var fragments = new string[] { "All is well" };

            // execute
            var result = StringAssembler.GreedyAssembleStrings(fragments);

            // test - can't guarantee end result, so just ensure all fragments are contained
            Assert.AreEqual(fragments[0], result);
        }

        [TestMethod]
        public void AssemblyFullyOverlappingSegmentsTestWith2()
        {
            // setup
            var fragments = new string[] { "All is well", "ell that en" };

            // execute
            var result = StringAssembler.GreedyAssembleStrings(fragments);

            // test - can't guarantee end result, so just ensure all fragments are contained
            Assert.AreEqual("All is well that en", result);
        }

        [TestMethod]
        public void AssemblyFullyOverlappingSegmentsTestWithMoreThan2()
        {
            // setup
            var fragments = new string[] { "All is well", "ell that en", "hat end", "t ends well" };

            // execute
            var result = StringAssembler.GreedyAssembleStrings(fragments);

            // test - we can guarantee the result because all the overlap sizes are unique, so no draws
            Assert.AreEqual("All is well that ends well", result);
        }
    }
}
