﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringAssembly;
using System.Linq;

namespace StringAssemblyTest
{
    [TestClass]
    public class OverlapGraphUtilsTests
    {
        [TestMethod]
        public void CalculateOverlapZeroTest()
        {
            //execute
            var result = OverlapGraphUtils.GetOverlap("ABCD", "EFG");

            // test
            Assert.AreEqual<int>(0, result);
        }

        [TestMethod]
        public void CalculateOverlapNonZeroTest()
        {
            //execute
            var result = OverlapGraphUtils.GetOverlap("ABCD", "CDEF");

            // test
            Assert.AreEqual<int>(2, result);
        }

        [TestMethod]
        public void GetGraphEdgeWithOrderedOverlapTest()
        {
            //execute
            var result = OverlapGraphUtils.GetGraphEdgeForStrings("ABCDE", "BCDE");

            //test
            Assert.AreEqual("ABCDE", result.BeginsWith);
            Assert.AreEqual("BCDE", result.EndsWith);
        }

        [TestMethod]
        public void GetGraphEdgeWithReversedOverlapTest()
        {
            //execute
            var result = OverlapGraphUtils.GetGraphEdgeForStrings("DEFG", "ABCDE");

            //test
            Assert.AreEqual("ABCDE", result.BeginsWith);
            Assert.AreEqual("DEFG", result.EndsWith);
        }

        [TestMethod]
        public void GetGraphEdgeWithOffsetGreaterThanSecondStringLength()
        {
            // execute
            var result = OverlapGraphUtils.GetGraphEdgeForStrings("STUVWXYZABCDE", "BCDE");

            //test
            Assert.AreEqual("STUVWXYZABCDE", result.BeginsWith);
            Assert.AreEqual("BCDE", result.EndsWith);
        }

        [TestMethod]
        public void GetGraphEdgeForLargeOverlap()
        {
            // execute
            var result = OverlapGraphUtils.GetGraphEdgeForStrings("ell that en", "hat end");

            // test
            Assert.AreEqual(6, result.OverlapSize);
        }

        [TestMethod]
        public void GetGraphEdgeForUnoverlappedFragment()
        {
            // execute
            var edge1 = OverlapGraphUtils.GetGraphEdgeForStrings("", "fragment");
            var edge2 = OverlapGraphUtils.GetGraphEdgeForStrings("fragment", "");

            // test
            Assert.AreEqual(0, edge1.OverlapSize);
            Assert.AreEqual(0, edge2.OverlapSize);
            Assert.AreEqual(string.Empty, edge1.BeginsWith);
            Assert.AreEqual(string.Empty, edge2.BeginsWith);
            Assert.AreEqual("fragment", edge1.EndsWith);
            Assert.AreEqual("fragment", edge2.EndsWith);
        }

        [TestMethod]
        public void EdgeConcatenatesWithOverlapTest()
        {
            // setup
            var edge = new GraphEdge("ABCDE", "CDEFG", 3);

            // execute
            var result = OverlapGraphUtils.ConcatenateEdge(edge);

            // Test
            Assert.AreEqual("ABCDEFG", result);
        }

        [TestMethod]
        public void EdgeConcatenatesWithoutOverlapTest()
        {
            // setup
            var edge = new GraphEdge("ABCDE", "CDEFG", 0);

            // execute
            var result = OverlapGraphUtils.ConcatenateEdge(edge);

            // Test
            Assert.AreEqual<string>("ABCDECDEFG", result);
        }

        [TestMethod]
        public void GetGraphEdgesWithNoZeroOverlapsTest()
        {
            // setup
            var fragments = new string[] { "ABCD", "BCDE", "DEFG" };

            // execute
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments);

            // test
            Assert.AreEqual<int>(3, edges.Count());
            Assert.IsTrue(edges.All(edge => edge.OverlapSize > 0));
        }

        [TestMethod]
        public void GetGraphEdgesWithZeroOverlapsTest()
        {
            // setup
            var fragments = new string[] { "ABCD", "CDEF", "EFGH", "XYZ", "123" };

            // execute
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments);

            // test
            Assert.AreEqual<int>(4, edges.Count());
            Assert.AreEqual<int>(2, edges.Count(edge => edge.OverlapSize == 0));
        }

        [TestMethod]
        public void GetGraphEdgesWithAllZeroOverlapsTest()
        {
            // setup
            var fragments = new string[] { "ABCD", "efgh", "IJKL", "mnop" };

            // execute
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments);

            // test
            Assert.AreEqual<int>(4, edges.Count());
            Assert.IsTrue(edges.All(edge => edge.OverlapSize == 0));
        }

        [TestMethod]
        public void GetGraphEdgesForTestData()
        {
            // setup
            var fragments = new string[] { "All is well", "ell that en", "hat end", "t ends well" };

            // execute
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments);

            Assert.AreEqual(4, edges.Count());
        }

        [TestMethod]
        public void RecalculateJoinedEdgeTest()
        {
            // setup
            var fragments = new string[] { "ABCD", "CDEF", "EFGH", "XYZ", "123" };
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments).ToList();

            // execute
            OverlapGraphUtils.RecalculateJoinedEdge(edges, edges.First());

            // test
            Assert.AreEqual<int>(3, edges.Count());
            Assert.AreEqual<int>(2, edges.Count(edge => edge.OverlapSize == 0));
        }

        [TestMethod]
        public void RecalculateWithOneLeftSideToMultipleRightSides()
        {
            // setup
            var fragments = new string[] { "One two three", "three four five", "three four six", "hamburgers" };
            var edges = OverlapGraphUtils.GetGraphEdgesForFragments(fragments).ToList();

            // execute
            OverlapGraphUtils.RecalculateJoinedEdge(edges, edges.First());

            // test
            Assert.AreEqual(3, edges.Count);
            Assert.IsTrue(edges.Contains(new GraphEdge(string.Empty, "One two three four five", 0)));
            Assert.IsTrue(edges.Contains(new GraphEdge(string.Empty, "three four six", 0)));
            Assert.IsTrue(edges.Contains(new GraphEdge(string.Empty, "hamburgers", 0)));
        }
    }
}
