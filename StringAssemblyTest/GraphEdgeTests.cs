﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringAssembly;

namespace StringAssemblyTest
{
    [TestClass]
    public class GraphEdgeTests
    {
        [TestMethod]
        public void NullIntoEmptyStringsTest()
        {
            // execute
            var edge = new GraphEdge(null, null, 0);

            // test
            Assert.AreEqual(string.Empty, edge.BeginsWith);
            Assert.AreEqual(string.Empty, edge.EndsWith);
        }

        [TestMethod]
        public void EmptyEqualityTest()
        {
            // setup
            var edge1 = new GraphEdge(null, null, 0);
            var edge2 = new GraphEdge(null, null, 0);

            // test
            Assert.AreEqual(edge1, edge2);
        }

        [TestMethod]
        public void NonEmptyEqualityTest()
        {
            // setup
            var edge1 = new GraphEdge("abc", null, 0);
            var edge2 = new GraphEdge("abc", null, 0);
            var edge3 = new GraphEdge(null, "abc", 0);
            var edge4 = new GraphEdge(null, "abc", 0);
            var edge5 = new GraphEdge("qwerab", "abc", 0);
            var edge6 = new GraphEdge("qwerab", "abc", 0);

            // test
            Assert.AreEqual(edge1, edge2);
            Assert.AreEqual(edge3, edge4);
            Assert.AreEqual(edge5, edge6);
        }

        [TestMethod]
        public void InequalityTest()
        {
            //setup
            var edge1 = new GraphEdge("abc", null, 0);
            var edge2 = new GraphEdge("def", null, 0);
            var edge3 = new GraphEdge(null, "abc", 0);
            var edge4 = new GraphEdge(null, "def", 0);
            var edge5 = new GraphEdge("defab", "abc", 2);
            var edge6 = new GraphEdge("asab", "abc", 2);

            //test
            Assert.AreNotEqual(edge1, edge2);
            Assert.AreNotEqual(edge3, edge4);
            Assert.AreNotEqual(edge5, edge6);
        }
    }
}
