﻿namespace StringAssembly
{
    /// <summary>
    /// GraphEdge represents the edge of an overlap graph. Its nodes are "BeginsWith" and "EndsWith" and
    /// "Overlap" indicates the length of the overlapping, respective, suffix and prefix.
    /// No checking is carried out on the truthiness of the data.
    /// </summary>
    public struct GraphEdge
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GraphEdge"/> struct.
        /// null values for strings are converted to string.Empty.
        /// The overlap size is ignored for equality tests.
        /// </summary>
        /// <param name="beginsWith">The string the edge begins with.</param>
        /// <param name="endsWith">The string the edge ends with.</param>
        /// <param name="overlapSize">Size of the overlapping suffix/prefix.</param>
        public GraphEdge(string beginsWith, string endsWith, int overlapSize)//, OverlapType overlapType)
        {
            this.BeginsWith = beginsWith?? string.Empty;
            this.EndsWith = endsWith?? string.Empty;
            this.OverlapSize = overlapSize;
        }

        public string BeginsWith { get; private set; }
        public string EndsWith { get; private set; }
        public int OverlapSize { get; private set; }

        /// <summary>
        /// Determines whether the edge contains a string value as either its
        /// BeginsWith or EndsWith values.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified node]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string node)
        {
            return BeginsWith == node || EndsWith == node;
        }

        /// <summary>
        /// Determins if this edge overlaps with the given edge. An edge overlaps with another edge
        /// if either of its nodes are the same as either of the other edge's nodes.
        /// </summary>
        /// <param name="edge">The edge.</param>
        /// <returns></returns>
        public bool Overlaps(GraphEdge edge)
        {
            return edge.OverlapSize > 0 && edge.Contains(BeginsWith) || edge.Contains(EndsWith);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is GraphEdge && ((GraphEdge)obj).BeginsWith == BeginsWith && ((GraphEdge)obj).EndsWith == EndsWith;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.BeginsWith.GetHashCode() ^ this.EndsWith.GetHashCode();
        }
    }
}
