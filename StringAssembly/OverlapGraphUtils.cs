﻿using System.Collections.Generic;
using System.Linq;

namespace StringAssembly
{
    /// <summary>
    /// Provides utilities to work with an "Overlap Graph". The idea of the overlap graph is
    /// adapted from an example on Coursera:
    /// https://www.coursera.org/learn/dna-sequencing/lecture/iD20p/practical-implementing-greedy-shortest-common-superstring
    /// by Ben Langmead and Jason Pritt from John Hopkins University.
    /// 
    /// The GetOverlap method is directly adapted, and the remaining code is original.
    /// </summary>
    public static class OverlapGraphUtils
    {

        /// <summary>
        /// Gets the length of the overlap of two strings.
        /// <paramref name="item1"/> is checked to see if it's suffix matches
        /// a prefix of equal lengh from <paramref name="item2"/>.
        /// </summary>
        /// <remarks>
        /// Adapted from an example on Coursera,
        /// by Ben Langmead and Jason Pritt from John Hopkins University
        /// </remarks>
        /// <param name="item1">The string to check the suffix of.</param>
        /// <param name="item2">The string to check the prefix of.</param>
        /// <returns>0 if there is no overlap, otherwise the length of the matching suffix/prefix</returns>
        public static int GetOverlap(string item1, string item2)
        {
            int start = 0;
            if (string.IsNullOrEmpty(item1) || string.IsNullOrEmpty(item2))
            {
                return 0;
            }

            while (true){
                // find the first char of item2
                start = item1.IndexOf(item2[0], start);

                if (start == -1)
                {
                    // first char of item2 is not even in item1, so no match
                    return 0;
                }

                // found a match - check for full suffix/prefix match
                if (item2.StartsWith(item1.Substring(start)))
                {
                    return item1.Length - start;
                }

                // keep looking just past the previous math
                start += 1;
            }
        }

        /// <summary>
        /// Calculates the graph edge for the two strings. An edge is always returned,
        /// even if it's overlap is 0, in which case only the second string will be added
        /// to an edge, and StartsWith will be set to the empty string.
        /// </summary>
        /// <param name="string1">The first string.</param>
        /// <param name="string2">The second string.</param>
        /// <returns>A graph edge with the calculated overlap and the strings ordered according
        /// to the overlap</returns>
        public static GraphEdge GetGraphEdgeForStrings(string string1, string string2)
        {
            var overlap1 = GetOverlap(string1, string2);
            var overlap2 = GetOverlap(string2, string1);
            if ((overlap1 > overlap2) ||
                ((overlap1 == overlap2) && overlap1 > 0))
            {
                return new GraphEdge(string1, string2, overlap1);
            }
            else if (overlap2 > overlap1)
            {
                return new GraphEdge(string2, string1, overlap2);
            }
            else
            {
                // overlap is 0 - prefer string 2 over string 1
                return new GraphEdge(string.Empty, string.IsNullOrEmpty(string2)? string1 : string2, 0);
            }
        }

        /// <summary>
        /// Concatenates the nodes of the edge into a single string, with the amount of
        /// specified overlap.
        /// No checking is performed to see if the overlap is accurate.
        /// </summary>
        /// <param name="edge">The edge to concatenate.</param>
        /// <returns>The concatenated string.</returns>
        public static string ConcatenateEdge(GraphEdge edge)
        {
            return edge.BeginsWith.Substring(0, edge.BeginsWith.Length - edge.OverlapSize) +
                edge.EndsWith;
        }

        /// <summary>
        /// Generates every unique combination of strings as a <seealso cref="GraphEdge"/>,
        /// measuring the overlap, even if 0 overlapped.
        /// Fragments with no other overlaps will be in their own edge with a zero overlap, and
        /// begin with an empty string
        /// </summary>
        /// <param name="items">The list of strings that make up the graph.</param>
        /// <returns>An <seealso cref="IEnumerable{T}"/> of the calculated <seealso cref="GraphEdge"/>s.</returns>
        public static IEnumerable<GraphEdge> GetGraphEdgesForFragments(string[] items)
        {
            // use a hash set to ignore duplicate edges
            HashSet<GraphEdge> result = new HashSet<GraphEdge>();
            HashSet<string> overlappingFragments = new HashSet<string>();

            if (items.Count() < 2)
            {
                return result;
            }

            // create each overlapping combination, measuring the overlap and create the
            // appropriate GraphEdge.
            for (int i = 0; i < items.Count(); i++)
            {
                for (int j = i + 1; j < items.Count(); j++)
                {
                    var edge = GetGraphEdgeForStrings(items[i], items[j]);
                    if (edge.OverlapSize > 0)
                    {
                        result.Add(edge);
                        overlappingFragments.Add(items[i]);
                        overlappingFragments.Add(items[j]);
                    }
                }
            }

            // add any unmatched fragments
            foreach (var item in items)
            {
                if (!overlappingFragments.Contains(item))
                {
                    result.Add(GetGraphEdgeForNonOverlappingString(item));
                }
            }

            return result;
        }

        /// <summary>
        /// Joins the strings of the edge and updates the overlap graph, recomparing the
        /// newly joined string to any other edges that had overlapped with one of the 
        /// pre-join strings.
        /// </summary>
        /// <param name="edges">The edges of the overlap graph.</param>
        /// <param name="edgeToJoin">The edge to Join.</param>
        public static void RecalculateJoinedEdge(List<GraphEdge> edges, GraphEdge edgeToJoin)
        {
            var joinedString = ConcatenateEdge(edgeToJoin);

            // remove the edge we are recalculating
            edges.Remove(edgeToJoin);

            // get the set of related edges that share either of the edge's nodes
            var edgesToUpdate = edges.Where(e => e.Overlaps(edgeToJoin)).ToList();

            // remove them from the main graph
            edges.RemoveAll(e => edgesToUpdate.Contains(e));

            // recompare the edges against the joined string
            List<GraphEdge> updatedOverlaps = RecompareOverlapsToJoinedEdge(edgeToJoin, edgesToUpdate);

            // add the updated edges
            edges.AddRange(updatedOverlaps);
        }

        /// <summary>
        /// Recompares the list of edges and generates new edges representing the updated overlap, if any.
        /// </summary>
        /// <param name="edgeToJoin">The edge being joined.</param>
        /// <param name="joinedString">The joined string.</param>
        /// <param name="edgesToCompare">The edges to compare.</param>
        /// <returns></returns>
        private static List<GraphEdge> RecompareOverlapsToJoinedEdge(GraphEdge edgeToJoin, List<GraphEdge> edgesToCompare)
        {
            var joinedString = ConcatenateEdge(edgeToJoin);

            // recalculate related edges against the newly joined string
            var newOverlaps = edgesToCompare.Select((e) =>
            {
                // NOTE: order is important here. If it's a 0 overlap, then only the second string is kept. See
                // documentation for GetGraphEdgeForStrings.
                return GetGraphEdgeForStrings(
                    joinedString,
                    edgeToJoin.Contains(e.BeginsWith)
                        ? e.EndsWith
                        : e.BeginsWith);
            }).ToList();

            // if there were no remaining overlaps with the joined string then add the newly joined string in too
            if (newOverlaps.All(edge => edge.OverlapSize == 0))
            {
                newOverlaps.Add(GetGraphEdgeForNonOverlappingString(joinedString));
            }

            return newOverlaps;
        }

        /// <summary>
        /// A convenience method that creates a correct graph edge for a fragment that doesn't overlap.
        /// </summary>
        /// <param name="fragment">The fragment.</param>
        /// <returns></returns>
        private static GraphEdge GetGraphEdgeForNonOverlappingString(string fragment)
        {
            return GetGraphEdgeForStrings(string.Empty, fragment);
        }
    }
}
