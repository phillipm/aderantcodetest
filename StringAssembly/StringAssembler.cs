﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringAssembly
{
    public static class StringAssembler
    {
        /// <summary>
        /// Assembles the string fragments into a single string using overlaps.
        /// Using a greedy common shortest superstring approach.
        /// </summary>
        /// <remarks>
        /// I didn't want to user the Stanford University searching algorithm because it 
        /// calculates the overlap for every permutation every time through the loop, i.e. n! times.
        /// Instead, I precalculate overlaps for all permutations at the start 
        /// (checking each combination both ways) and then only recheck edges that share the same
        /// fragment as an edge that gets it's strings concatenated.
        /// It's not clear if this is intended for parsing large data sets, and I don't want to try
        /// to optimise prematurely, so I'm just using List and HasSet containers.
        /// </remarks>
        /// <param name="fragments">The string fragments to be assembled.</param>
        /// <returns>The assembled string.</returns>
        public static string GreedyAssembleStrings(string[] fragments)
        {
            if (fragments == null)
                return string.Empty;

            if (fragments.Length == 1)
            {
                // there is only one fragment
                return fragments[0];
            }

            // remove duplicates since the shortest result would just absorb them into into each other anyway
            var uniqueSet = new HashSet<string>(fragments.ToList()).ToArray();

            //calculate the graph edges of overlapping fragments, sorted by overlap size
            var graphEdges = OverlapGraphUtils.GetGraphEdgesForFragments(uniqueSet).ToList();
            
            // assemble the edges by just taking the edge with the largest overlap each time
            while (graphEdges.Count() > 1)
            {
                var orderedEdges = graphEdges.OrderByDescending(edge => edge.OverlapSize);
                OverlapGraphUtils.RecalculateJoinedEdge(graphEdges, orderedEdges.First());
            }

            //the last remaining edge now needs to be concatenated. Handle case of empty fragments
            var result = !graphEdges.Any() ? string.Empty : OverlapGraphUtils.ConcatenateEdge(graphEdges.First());

            // append any fragments that did not appear in the edge graph (i.e. there were no overlaps)
            var builder = new StringBuilder(result);
            fragments.ToList().ForEach(fragment =>
            {
                if (result.IndexOf(fragment) == -1)
                {
                    builder.Append(fragment);
                }
            });

            return builder.ToString();
        }
    }
}
