﻿using StringAssembly;
using System.Threading.Tasks;
using System.Windows;

/// <summary>
/// Since the Coding test is not about the UI, and this UI is super-trivial, I won't bother
/// with a view and view model (etc.). Otherwise I would be using MVVM or MVPVM and injecting with Unity.
/// </summary>
namespace AderantCodingTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= this.MainWindow_Loaded;
            this.Fragments.Text = "All is well\nell that en\nhat end\nt ends well";
        }

        /// <summary>
        /// Performs the calculation. This is hacky code because it's incidental to the code test.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private async void Assemble_Click(object sender, RoutedEventArgs e)
        {
            this.Assemble.IsEnabled = false;
            var fragments = this.Fragments.Text.Replace("\r\n","\n").Split('\n');
            var isMultiple = this.IsMultiple.IsChecked;
            long runtime = 0;
            int runAmount = 1;
            int.TryParse(this.RunAmount.Text, out runAmount);
            this.RunTime.Text = "running";
            // wrap it in a task to keep the ui responsive in case the amount is large.
            var result = await Task.Run(() => {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                if (isMultiple == true && runAmount > 1)
                {
                    for (int i = 0; i < runAmount-1; i++)
                    {
                        StringAssembler.GreedyAssembleStrings(fragments);
                    }
                }

                var joined = StringAssembler.GreedyAssembleStrings(fragments);
                watch.Stop();
                runtime = watch.ElapsedMilliseconds;
                return joined;
            });

            this.Assembled.Text = result;
            this.RunTime.Text = $"{(int)runtime} ms";
            this.Assemble.IsEnabled = true;
        }
    }
}
